// for compression
const zlib = require('zlib');

// for DynamoDB
const AWS = require("aws-sdk");
const ddb = new AWS.DynamoDB.DocumentClient();

// for Slack
const Slack = require('slack-node');
const slack = new Slack();

// static variables
const ddb_table = 'isms-access-audit-logging';
const webhookUri = 'https://hooks.slack.com/services/...';

Slack.prototype.sendmessage = (message) => {
    slack.webhook(
        {
            "text": message,
        }, (err, response) => {
            if (err) { console.log(response); }
            else { console.log(response);}
        });
}

const parseEventMessage = (payload) => {
    const result = payload.split(',');
    return {
        "timestamp": result[0],
        "instance": result[1],
        "account": result[2],
        "ip": result[3],
        "connect": result[6],
    }
}

exports.handler = (input, context) => {
    slack.setWebhook(webhookUri);

    if (input.awslogs) {
        const payload = Buffer.from(input.awslogs.data, 'base64');
        zlib.gunzip(payload, (e, result) => {
            try {
                if (e) {
                    console.log('Fail', e);
                } else {
                    result = JSON.parse(result.toString());

                    for (let ev of result.logEvents) {
                        const payload = parseEventMessage(ev.message);

                        // Save to DynamoDB
                        ddb.put({
                            TableName : ddb_table,
                            Item: {
                                Account: payload.account,
                                Timestamp: ev.timestamp,
                                Action: payload.connect,
                                Instance: payload.instance,
                                UserIP: payload.ip
                            }
                        }).promise();

                        // Send message to Slack App
                        let message = '====================================================\n';
                        message += payload.connect === 'CONNECT' ? '- Connect' : '- Disconnect';
                        message += ' : ' + payload.account + ' ( ' + payload.ip + ' ) ';
                        message += "\n" + '- Target DB: ' + payload.instance;
                        message += "\n" + '- Timestamp : ' + new Date(ev.timestamp);

                        slack.webhook(
                            {
                                "text": message,
                            }, (err, response) => {
                                if (err) { console.log('slack webhook error:', response); }
                                else { console.log('slack webhook success:',response);}
                            });
                    }

                    console.log('Event Data >>>>:', JSON.stringify(result, null, 2));
                }
            } catch (err) {
                console.error('Error has occurred:', err);
            }
        });
    }
    return "SUCCESS";
}

